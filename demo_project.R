library(shiny)
ui <- fluidPage(
  titlePanel("This is a demo app"),
  sidebarLayout(
    sidebarPanel(
      textInput("txtInput","Input the text to display")
    ),
    mainPanel(
      paste("You are entering"),
      textOutput("txtOutput")
    )
  )
)

server <- shinyServer(function(input,output){
  output$txtOutput <- renderText({
    paste(input$txtInput)
  })
})


shinyApp(ui=ui, server=server)
